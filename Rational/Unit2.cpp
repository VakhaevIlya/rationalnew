//---------------------------------------------------------------------------

#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------


#pragma hdrstop

#include "Unit2.h"
#include <sstream>
//---------------------------------------------------------------------------

#pragma package(smart_init)
using namespace std;
int Rational::nod(int d1,int d2)
{
        int tmp;
        d1=abs(d1);
        d2=abs(d2);
		while ((d1 > 0) && (d2 > 0))
        {
                tmp=d1;
				d1 = d2%d1;
                d2=tmp;
        }
		if ((d1 == 0) && (d2 == 0))
			return 1;
		else
		return d1 + d2;
}

int Rational::nok(int d1, int d2)
{
	return d1 / nod(d1, d2) * d2;
}

double Rational::toDouble() const
{
	return double(n) / m;
}
void Rational::alignSign()
{
	if (this->m < 0)
	{
		this->n = -this->n;
		this->m = -this->m;
	}
	if (this->n == 0)
		this->m = 1;
}
Rational::Rational(int n,int m)
{
	if (m == 0)
		throw invalid_argument("Zero denominator");
	else
	{
		this->n = n / nod(n, m);
		this->m = m / nod(n, m);
		alignSign();
	}
}
Rational &Rational::operator+=(const Rational& d)
{
	int q;
	q = nok(this->m, d.m);
	this->n = this->n * (q / this->m) + d.n * (q / d.m);
	this->m = q;
		alignSign();
        return *this;
}

Rational &Rational::operator-=(const Rational& d)
{
	int q;
	q = nok(this->m, d.m);
	this->n = this->n * (q / this->m) - d.n * (q / d.m);
	this->m = q;
		alignSign();
        return *this;
}

Rational &Rational::operator*=(const Rational& d)
{
        int p1, q1, p2, q2, tmp;
		p1 = this->n;
		q1 = this->m;
		p2 = d.n;
		q2 = d.m;

		tmp = nod(p1, q2);
		p1 /= tmp;
		q2 /= tmp;

		tmp = nod(p2, q1);
		p2 /= tmp;
		q1 /= tmp;

		this->n = p1*p2;
		this->m = q1*q2;
		alignSign();

		return *this;
}

Rational &Rational::operator/=(const Rational& d)
{
	int p1, q1, p2, q2, tmp;
	p1 = this->n;
	q1 = this->m;
	p2 = d.m;
	q2 = d.n;

	tmp = nod(p1, q2);
	p1 /= tmp;
	q2 /= tmp;

	tmp = nod(p2, q1);
	p2 /= tmp;
	q1 /= tmp;

	this->n = p1*p2;
	this->m = q1*q2;
	alignSign();
	return *this;
}

Rational Rational::operator+(const Rational &d) const
{
	Rational tmp(this->n, this->m);
	tmp += d;
	return tmp;
}

Rational Rational::operator-(const Rational &d) const
{
	Rational tmp(this->n, this->m);
	tmp -= d;
	return tmp;
}

Rational Rational::operator*(const Rational &d) const
{
	Rational tmp(this->n, this->m);
	tmp *= d;	
	return tmp;
}

Rational Rational::operator/(const Rational &d) const
{
	Rational tmp(this->n, this->m);
	tmp /= d;
	return tmp;
}

Rational &Rational::operator=(const Rational &d)
{
	if (this != &d)
	{
		this->n = d.n;
		this->m = d.m;
	}
	return *this;
}

ostream& operator<<(ostream& o, const Rational &d)
{
	o << d.getN() << "/" << d.getM() � endl;
	return o;
}

istream& operator>>(istream& o,Rational &d)
{
        int m,n;
        char c;
        o>>n;
        o>>c;
        o>>m;
        Rational dd(n,m);
        d=dd;
        return o;
}