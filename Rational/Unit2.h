//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <string.h>
#include <iostream>
//---------------------------------------------------------------------------
using namespace std;
class Rational
{
private: 
	    static int nod(int d1, int d2); 
		static int nok(int d1, int d2);
		void alignSign();
        int n,m;
public:
        int getN() const {return n;};
        int getM() const {return m;};
        Rational(int n,int m);
        double toDouble() const; 
        Rational &operator+=(const Rational& d);
        Rational &operator-=(const Rational& d);
        Rational &operator*=(const Rational& d);
        Rational &operator/=(const Rational& d);
        Rational &operator=(const Rational& d);
		Rational operator+(const Rational &d) const;
		Rational operator-(const Rational &d) const;
		Rational operator*(const Rational &d) const;
		Rational operator/(const Rational &d) const;
};
ostream& operator<<(ostream& o, const Rational &d);
istream& operator>>(istream& o, Rational &d);


#endif
 